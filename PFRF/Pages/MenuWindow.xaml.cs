﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PFRF.Pages
{
    /// <summary>
    /// Логика взаимодействия для MenuWindow.xaml
    /// </summary>
    public partial class MenuWindow : Window
    {
        public MenuWindow()
        {
            InitializeComponent();
        }

        private void Raschet_Click(object sender, RoutedEventArgs e)
        {
            RaschetWindow taskWindow = new RaschetWindow();
            taskWindow.Show();
        }

        private void LichDan_Click(object sender, RoutedEventArgs e)
        {
            LichDanWindow taskWindow = new LichDanWindow();
            taskWindow.Show();
        }

        private void DopSved_Click(object sender, RoutedEventArgs e)
        {

        }

        private void SvedRab_Click(object sender, RoutedEventArgs e)
        {

        }

        private void SvedDost_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
